<?php

namespace App\Enums;

class RoleEnums
{
    CONST COACH = 1;
    CONST COACH_PLAYER = 2;
    CONST PLAYER = 3;
}
