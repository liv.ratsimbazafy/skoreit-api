<?php

namespace App\Http\Controllers\Api;

use App\Enums\RoleEnums;
use App\Http\Controllers\Controller;
use App\Http\Resources\TeamResource;
use App\Jobs\EmailVerificationJob;
use App\Jobs\PlayerCreateJob;
use App\Jobs\TeamCreateJob;
use App\Models\Category;
use App\Models\Event;
use App\Models\EventDetail;
use App\Models\Team;
use App\Models\Type;
use App\Models\User;
use App\Models\UserDetail;
use App\Notifications\EmailVerificationNotification;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     */
    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'team_name' => 'required|string',
            'sport_type_id' => 'required|digits:1',
            'sport_category_id' => 'required|digits:1',
            'season_start' => 'required',
            'season_end' => 'required'
        ]);

        if ($validator->fails()) {
            $message = $validator->errors()->first();
            return response()->json(['data' => ['message' => $message]], 422);
        }

        $existing_team = Team::where('name', '=', $request->team_name)->first();

        if (!is_null($existing_team))
            return response()->json(['data' => ['message' => 'Une équipe au même nom existe déja !']], 422);

        if (!$user = Auth::user())
            return response()->json(['data' => ['message' => 'user not found']], 422);

        try {
            $team = new Team();
            $team->types_id = $request->sport_type_id;
            $team->categories_id = $request->sport_category_id;
            $team->name = $request->team_name;
            $team->save();

            $team->team_details()->create([
                'season_start' => $request->season_start,
                'season_end' => $request->season_end,
                'user_id' => Auth::id(),
                'is_current_season' => true,
            ]);

            $user->user_details()->create([
                'user_id' => Auth::id(),
                'team_id' => $team->id,
                'role_id' => RoleEnums::COACH,
                'season_start' => $request->season_start,
                'season_end' => $request->season_end,
                'current_season' => date('Y'),
                'join_on' => Carbon::now()
            ]);

            $user->teams()->attach($team->id);

            TeamCreateJob::dispatch($user, $request->team_name);

            return response()->json(['data' => ['user' => $user]], 201);

        } catch (\Exception $e) {
            return response()->json(['data' => ['message' => $e->getMessage()]], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Team $team
     */
    public function show(Team $team, Request $request): TeamResource
    {
        /*@todo get only team with current season true*/
        return new TeamResource($team);
    }

    public function edit(Request $request, Team $team)
    {
        $validator = Validator::make($request->only(['name', 'types_id', 'categories_id', 'description', 'city']), [
            'name' => 'required|min:2|max:100',
            'types_id' => 'numeric|required',
            'categories_id' => 'numeric|required',
            'description' => 'min:5|max:200|nullable',
            'city' => 'min:5|max:200|nullable',
        ]);

        if ($validator->fails())
            return response()->json(['data' => ['message' => $validator->errors()->first()]], 422);

        $team->fill([
            'name' => $request->name,
            'types_id' => $request->types_id,
            'categories_id' => $request->categories_id,
            'description' => $request->description,
            'city' => $request->city
        ]);
        $team->save();

        return response()->noContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Team $team
     * @return void
     */
    public function destroy(Team $team)
    {
        //
    }

    public function getTypesAndCategories(): JsonResponse
    {
        return response()->json([
            'data' => [
                'sports' => Type::all(),
                'categories' => Category::all()
            ]
        ], 200);
    }

    public function getPlayers(Team $team, Request $request): JsonResponse
    {
        $page = true;
        if ($request->has('page') && $request->query('page') == 'false')
            $page = false;
        return response()->json($team->getCurrentSeasonPlayers($page));
    }

    public function getPlayerInfos($team_id, $player_id): JsonResponse
    {
        /*$player = User::where('id', $player_id)->with(['user_details' => function ($q) use ($team_id, $player_id) {
            $q->where('team_id', $team_id)->where('user_id', $player_id);
        }])->first();*/

        $user = User::find($player_id);
        $shopping_details = EventDetail::whereHas('event', function ($q) use ($user, $team_id) {
            $q->where('user_id', $user->id)
                ->where('team_id', $team_id)
                ->where('invoice_amount', '!=', 0);
        })->orderBy('invoice_amount', 'DESC')->get();

        return response()->json(
            ['data' =>
                [
                    'player' => $user,
                    'player_details' => $user->user_details()->where('team_id', $team_id)->first(),
                    'events' => $user->events()->with('event_details')->where('events.team_id', $team_id)->get(),
                    'shopping' => $shopping_details
                ]
            ]);
    }

    public function addPlayers(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'players' => 'required|array',
            'players.*.name' => 'required',
            'players.*.email' => 'required|email',
            'team_id' => 'required',
            'send_email' => 'boolean'
        ]);

        if ($validator->fails())
            return response()->json(['data' => ['message' => $validator->errors()->first()]], 422);

        try {
            $team = Team::find($request->team_id);
            $team_details = $team->team_details->where('is_current_season', true)->first();
            $secret = uniqid() . '_' . $request->team_id;
            $txt_pwd = null;

            foreach ($request->players as $key => $player) {

                $user = User::where('email', $player['email'])->first();

                // check if user is already in this team
                if ($user && $user->user_details()->exists()) {
                    if ($user->user_details->first()->team_id == $request->team_id)
                        return response()->json(['data' => ['message' => 'Un joueur avec le même email' . $key . ' existe déjà']], 422);
                }

                if (is_null($user)) {
                    $secret = null;
                    $txt_pwd = Str::random(15);
                    $user = new User();
                    $user->name = $player['name'];
                    $user->email = $player['email'];
                    $user->password = Hash::make($txt_pwd);
                    $user->profile_img = "https://ui-avatars.com/api/?name=" . $user->name;
                    $user->save();
                }

                $user->teams()->attach($request->team_id);

                $user->user_details()->create([
                    'user_id' => $user->id,
                    'team_id' => $request->team_id,
                    'role_id' => RoleEnums::PLAYER,
                    'secret' => $secret,
                    'season_start' => $team_details->season_start,
                    'season_end' => $team_details->season_end,
                    'current_season' => date('Y'),
                    'join_on' => Carbon::now()
                ]);

                PlayerCreateJob::dispatch($user, $team->name, $txt_pwd);
            }
            return response()->json(['data' => ['message' => 'Action performed successfully']], 201);

        } catch (\Exception $e) {
            return response()->json(['data' => ['message' => $e->getMessage(), 'error' => $request->team_id]], 422);
        }
    }

    public function removePlayer(Request $request): JsonResponse
    {
        $validator = Validator::make($request->only('team_id', 'player_id'), [
            'team_id' => 'required',
            'player_id' => 'required',
        ]);
        if ($validator->fails())
            return response()->json(['data' => ['message' => $validator->errors()->first()]], 422);

        $user = User::find($request->player_id);

        if ($user) {
            $user->user_details()->where('team_id', $request->team_id)->delete();
            $user->teams()->detach($request->team_id);
        }

        return response()->json(['data' => ['message' => 'Removed successfully']]);
    }

    public function getTeamGlobalStats(Request $request, $team_id): JsonResponse
    {
        foreach (Team::find($team_id)->users as $user) {

            $player_with_events[] = User::where('id', $user->id)->with(['events', 'user_details' => function ($q) use ($team_id) {
                $q->where('user_details.team_id', $team_id);
            }])->first();
        }

        //$current_season_events = Team::find($team_id)->events;
        $team = Team::find($team_id);
        $current_season_events = $team->events()->paginate(5);
        $player_events = Auth::user()->events()->where('event_user.team_id', $team_id)->get();
        return response()->json(
            ['data' =>
                [
                    'all_events' => $current_season_events,
                    'players' => $player_with_events,
                    'total_event' => count($current_season_events),
                    'total_player' => count($player_with_events),
                    'player_events' => $player_events
                ]
            ]);
    }

    public function editTeamDetailContribution(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'team_id' => 'required',
            'contribution_price' => 'required',
        ]);

        if ($validator->fails())
            return response()->json(['data' => ['message' => $validator->errors()->first()]], 422);

        if (!$team = Team::find($request->team_id))
            return response()->json(['data' => ['message' => 'No team found']], 422);

        // find user details with team id & total attendances != 0 && contribution total != 0
        $team_players = UserDetail::where('team_id', $request->team_id)
            ->where('total_attendances', '>', 0)
            ->where('contribution_total', '>', 0)
            ->get();
        // grab users total attendances
        if (!is_null($team_players)) {
            foreach ($team_players as $player) {
                $player->update([
                    'contribution_total' => $player->total_attendances * $request->contribution_price,
                    'contribution_unpaid' => $player->total_attendances * $request->contribution_price,
                ]);
            }
        }
        // foreach user => update contribution_total = total_attendances * $request->contribution_price
        $team->team_details()->update([
            'contribution_price' => $request->contribution_price
        ]);

        return response()->noContent();
    }
}
