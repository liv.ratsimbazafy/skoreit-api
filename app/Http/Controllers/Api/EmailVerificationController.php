<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\EmailVerificationJob;
use App\Models\User;
use App\Notifications\EmailVerificationNotification;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class EmailVerificationController extends Controller
{
    use VerifiesEmails;

    /**
     * Handle email verification
     */
    public function verify(Request $request)
    {
        if(! Auth::user() || ! hash_equals((string) $request->route('hash'), sha1($request->user()->getEmailForVerification())))
            //return redirect('http://localhost:3000/email/verification/sent?authenticated=false&user=' . $request->route('id'));
            return redirect(env('CLIENT_BASE_URL') . '/email/verification/sent?authenticated=false&user=' . $request->route('id'));

        $user = User::find($request->route('id'));

        if ($user->hasVerifiedEmail())
            return redirect(env('CLIENT_BASE_URL') . '/email/already-verified');

        if ($user->markEmailAsVerified())
            event(new Verified($user));

        return redirect(env('CLIENT_BASE_URL') . '/email/verification/success/' . $user->id);
    }

    /**
     * resend the email confirmation email
     */
    public function resend(Request $request): JsonResponse
    {
        if (Auth::user()->hasVerifiedEmail()) {
            return response()->json('Désolé, cette adresse email a déjà été validé!', 422);
        }

        EmailVerificationJob::dispatch(Auth::user());

        return response()->json('Un nouveau email de confirmation vous a été renvoyé!');
    }
}
