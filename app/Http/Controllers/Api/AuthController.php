<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\EmailVerificationNotification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use PHPUnit\Exception;

class AuthController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     *
     */
    public function authenticate(Request $request): JsonResponse
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            //@todo return user basic resource( id, profile_img, team_id )
            return response()->json(['data' => [
                'user' => Auth::user()
            ]]);
        }

        return response()->json(['data' => ['message' => 'The provided email do not match our records.']], 422);
    }


    public function register(Request $request): JsonResponse
    {
        $validator = Validator::make($request->only(['name', 'email', 'password']), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:5'
        ]);

        if ($validator->fails())
            return response()->json(['data' => ['message' => $validator->errors()->first()]], 422);

        try {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'profile_img' => "https://ui-avatars.com/api/?name=" . $request->name
            ]);

            $this->handleResignIn($request, $user);

            return response()->json(['data' => ['user' => $user]], 201);
        }
        catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function userResignIn(Request $request): JsonResponse
    {
        $user = User::find($request->user_id);

        if(!$user)
            return response()->json(['data' => ['message' => 'No user found']], 422);

        $this->handleResignIn($request, $user);

        return response()->json(['data' => Auth::user()]);
    }

    public function handleResignIn($request, $user)
    {
        Auth::login($user);
        $user->notify(new EmailVerificationNotification($user));
        $request->session()->regenerate();

        return $user;
    }

    public function passwordUpdate(Request $request): JsonResponse
    {
        $validator = Validator::make($request->only(['user_id', 'password', 'new_password']), [
            'user_id' => 'required',
            'password' => 'required|min:5',
            'new_password' => 'required|min:5'
        ]);

        if ($validator->fails())
            return response()->json(['data' => ['message' => $validator->errors()->first()]], 422);

        try {
            $user = User::find($request->user_id);

            if($user && !Hash::check($request->password, $user->password))
                return response()->json(['data' => ['message' => 'Mot de passe incorrecte !']], 422);

            $user->update(['password' => Hash::make($request->new_password)]);

            return response()->json('', 204);
        }
        catch (\Exception $e) {
            return response()->json(['data' => ['message' => $e->getMessage()]], $e->getCode());
        }
    }

    public function profileImageUpdate(Request $request): JsonResponse
    {
        $validator = Validator::make($request->only(['avatar', 'user_id']), [
            'avatar' => 'required|mimes:png,jpg,jpeg|max:10000',
            'user_id' => 'required'
        ]);

        if ($validator->fails())
            return response()->json(['data' => ['message' => $validator->errors()->first()]], 422);

        $user = User::find($request->user_id);

        if(is_file($user->profile_img))
            File::delete($user->profile_img);

        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $extension = $avatar->extension();
            $avatar_name = trim($user->name) . '.' . $extension;

            Storage::disk('public')->putFileAs('users_profiles', $avatar, $avatar_name);

            $user->update([
                'profile_img' => url('storage/users_profiles/' . $avatar_name)
            ]);
        }

        return response()->json(['data' => ['user' => $user]]);
    }
}
