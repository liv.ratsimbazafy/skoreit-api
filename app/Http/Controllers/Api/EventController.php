<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\EventResource;
use App\Models\Event;
use App\Models\EventDetail;
use App\Models\Team;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    public function index(int $team_id): JsonResponse
    {
        $team = Team::find($team_id);
        if (is_null($team->events))
            return response()->json(['message' => 'no events found for this team'], 422);

        return response()->json(['data' => [
            'upcoming_events' => $team->getUpcomingEvents(),
            'passed_events' => $team->getPassedEvents(),
            'all_events' => $team->events()->paginate(5)
        ]], 200);
    }


    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make($request->only('team_id', 'name', 'description', 'venue', 'time', 'address'), [
            'team_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'venue' => 'required',
            'time' => 'required',
            'address' => 'required'
        ]);

        if ($validator->fails()) {
            $message = $validator->errors()->first();
            return response()->json(['data' => ['message' => $message]], 422);
        };
        if (Event::where('venue', '=', $request->venue)->where('time', '=', $request->time)->first())
            return response()->json(['data' => ['message' => "Un évènement à la même heure existe déjà!"],
            ], 422);

        try {
            $event = new Event();
            $event->team_id = $request->team_id;
            $event->name = $request->name;
            $event->description = $request->description;
            $event->venue = $request->venue;
            $event->time = "" . $request->time . "";
            $event->address = $request->address;
            $event->season_id = 1;
            $event->save();

            return response()->json([], 201);

        } catch (Exception $e) {
            return response()->json(['data' => ['message' => $e->getMessage()]], 422);
        }
    }

    public function show(Event $event): EventResource
    {
        return new EventResource($event);
    }


    public function update(Request $request, Event $event): JsonResponse
    {
        $validator = Validator::make($request->only('name', 'description', 'venue', 'time', 'address'),
            [
                'name' => 'required',
                'description' => 'required',
                'venue' => 'required',
                'time' => 'required',
                'address' => 'required'
            ]);

        if ($validator->fails())
            return response()->json(['data' => ['message' => $validator->errors()->first()]], 422);

        try {
            $event->fill([
                'name' => $request->name,
                'description' => $request->description,
                'venue' => $request->venue,
                'time' => $request->time,
                'address' => $request->address,
            ]);

            $event->save();

            return response()->json([], 200);

        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }


    public function destroy(Team $team, Event $event)
    {
        try {
            $user = Auth::user();

            $event->users()->detach($user->id);

            $this->updateUserDetails($team->id, null);

            $event->delete();

            return response()->noContent();
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    public function addPlayerToEvent(Event $event, Team $team)
    {
        try {
            if (Auth::user()->events()->where('events.id', $event->id)->exists())
                return response()->json(['data' => ['message' => 'User already present !']]);

            $event->users()->attach(Auth::id(), ['team_id' => $team->id]);

            $this->updateUserDetails($team->id, null);

            return response()->noContent();
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    public function removePlayerFromEvent(Event $event, Team $team)
    {
        try {
            $event->users()->detach(Auth::id());
            $this->updateUserDetails($team->id, null);
            return response()->noContent();
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    public function addEventTaskContribution(Request $request, Event $event)
    {
        $validator = Validator::make($request->only('team_id', 'type', 'invoice_amount'),
            [
                'team_id' => 'required',
                'type' => 'required',
                'invoice_amount' => 'required',
            ]);

        if ($validator->fails())
            return response()->json(['data' => ['message' => $validator->errors()->first()]], 422);

        try {
            $player_tasks = $event->event_details()
                ->where('user_id', '=', Auth::id())
                ->where($request->type, true)
                ->first();

            if (is_null($player_tasks))
                return response()->json(['message' => 'No player tasks found !']);

            $player_tasks->fill([
                'invoice_amount' => $request->invoice_amount
            ]);
            $player_tasks->save();

            $this->updateUserDetails($request->team_id, $request->invoice_amount);

            return response()->noContent();
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }

    public function handleEventTasks(Request $request, Event $event): JsonResponse
    {
        if (!$request->has('type') && !$request->has('action'))
            return response()->json(['data' => ['message' => 'Query parameters are required']], 422);

        $type = $request->query('type');
        $action = $request->query('action');

        try {
            $player_tasks = $event->event_details()
                ->where('user_id', '=', Auth::id())
                ->where($type, true)
                ->get();

            if ($action == "ADD") {
                foreach ($player_tasks as $player_task) {
                    if (!is_null($player_task))
                        return response()->json(['data' => ['message' => 'You are already in charge of ' . $type]], 422);
                }
                $event->event_details()->create([
                    'user_id' => Auth::id(),
                    $type => true
                ]);
            } else if ($action == "REMOVE") {
                foreach ($player_tasks as $player_task) {
                    if (is_null($player_task)) {
                        return response()->json(['data' => ['message' => 'No task ' . $type . ' found !']], 422);
                    }
                    $player_task->delete();
                }
            } else {
                return response()->json(['data' => ['message' => 'Action not allowed !']], 401);
            }

            return response()->json(['data' => [
                'users' => $event->getPlayersSubscription($type),
                'type' => $type,
            ]], 200);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }

    }

    public function updateUserDetails($team_id, $invoice_amount)
    {
        try {
            $user = Auth::user();
            $team = Team::find($team_id);

            $team_events = Event::where('team_id', $team->id)->count();
            $user_events = $user->events()->where('events.team_id', $team_id)->count();
            $event_price = $team->team_details->first()->contribution_price;
            $total_contribution = $user_events * $event_price;

            $user_detail = $user->user_details->where('team_id', $team_id)->where('user_id', $user->id)->first();

            $paid = !is_null($invoice_amount) ? $user_detail->contribution_paid + $invoice_amount : $user_detail->contribution_paid;
            $refund = max($paid - $total_contribution, 0);
            $unpaid = max(($user_events * $event_price) - $paid, 0);

            $user_detail->update([
                'total_attendances' => $user_events,
                'contribution_total' => $total_contribution,
                'contribution_unpaid' => $unpaid,
                'contribution_paid' => $paid,
                'attendance_rate' => round(($user_events / $team_events) * 100, 2),
                'refund' => $refund
            ]);

            $user_detail->save();
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], $e->getCode());
        }
    }
}
