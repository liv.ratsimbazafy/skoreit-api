<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function updateUser(Request $request): JsonResponse
    {
        $validator = Validator::make($request->only('team_id', 'player_id', 'name', 'email'), [
            'team_id' => 'required',
            'player_id' => 'required',
            'name' => ['required', Rule::unique('users')->ignore($request->player_id)],
            'email' => ['required', Rule::unique('users')->ignore($request->player_id)],
        ]);
        if ($validator->fails())
            return response()->json(['data' => ['message' => $validator->errors()->first()]], 422);

        $user = User::find($request->player_id);
        if($user->email === $request->email)
            $user->update(['name' => $request->name]);

        $user->update(['name' => $request->name, 'email' => $request->email]);
        return response()->json(['data' => $user], 200);
    }

    public function updateUserDetails(Request $request): JsonResponse
    {
        $validator = Validator::make($request->only('team_id',
            'player_id',
            'nick_name',
            'date_of_birth',
            'height',
            'weight',
            'jersey_number'),
            [
                'team_id' => 'required',
                'player_id' => 'required',
                'nick_name' => 'required',
                'date_of_birth' => 'required',
                'height' => 'required',
                'weight' => 'required',
                'jersey_number' => 'required',
            ]);
        if ($validator->fails())
            return response()->json(['data' => ['message' => $validator->errors()->first()]], 422);

        UserDetail::where('team_id', $request->team_id)->where('user_id', $request->player_id)->update([
            'nick_name' => $request->nick_name,
            'date_of_birth' => $request->date_of_birth,
            'height' => $request->height,
            'weight' => $request->weight,
            'jersey_number' => $request->jersey_number,
        ]);

        return response()->json(['data' => ['message' => 'User infos was successfully updated']], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
