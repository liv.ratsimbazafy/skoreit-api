<?php

namespace App\Http\Resources;

use App\Models\UserDetail;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user' => [
                'id' => $this->id,
                'email' => $this->email,
                'name' => $this->name,
                'profile_img' => $this->profile_img,
                'team_id' => !is_null($this->getTeam()) ? $this->getTeam()->team_id : 'Joueur non activé !', //@todo fix it
                'role' => is_null($this->team_id) ? $this->getTeam()->role : $this->getRoleInTeam($this->team_id),
            ],
            'user_teams' => $this->teams
        ];
    }

    public function getRoleInTeam($team_id)
    {
        return UserDetail::where('user_id', $this->id)
            ->where('team_id', $team_id)
            ->first()->role;
    }

    public function getTeam() {
        return $this->user_details->whereNull('secret')->first();
    }
}
