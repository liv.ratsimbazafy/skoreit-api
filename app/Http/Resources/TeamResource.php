<?php

namespace App\Http\Resources;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class TeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'team' => [
                'id' => $this->id,
                'name' => $this->name,
                'description' => $this->description,
                'season_start' => $this->team_details->first()->season_start,
                'season_end' => $this->team_details->first()->season_end,
                'sport' => $this->type->name,
                'category' => $this->categories->name,
                'created_at' => $this->created_at,
                'city' => $this->city,
                'coach' => $this->getTeamCoach(),
                'players' => $this->getCurrentSeasonPlayers(false),
                'all_players' => $this->users,
                'team_details' => $this->team_details->first()
            ],
            'user_teams' => $this->getActivePlayerTeams(), /*@todo find why it is used*/
            'events' => [
                'total' => $this->events->count(),
                'upcoming_events' => $this->getUpcomingEvents(),
                'passed_events' => $this->getPassedEvents()
            ]
        ];
    }

    public function getTeamCoach()
    {
        $team = $this->team_details->where('is_current_season', true)->first();
        return $team->user;
    }

    private function getActivePlayerTeams()
    {
        $user = Auth::user();
        $team_ids = [];

        foreach ($user->user_details() as $team) {
            $team_ids[] = $team->team_id;
        }

        return $user->teams->whereNotIn('id', $team_ids)->values();
    }
}
