<?php

namespace App\Http\Resources;

use App\Models\Event;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'event' => [
                'id' => $this->id,
                'team_id' => $this->team_id,
                'season_id' => $this->season_id,
                'name' => $this->name,
                'description' => $this->description,
                'venue' => $this->venue,
                'time' => $this->time,
                'address' => $this->address,
            ],
            'event_details' => $this->event_details->where('user_id', '=', Auth::id())->all(),
            'players_in' => $this->users,
            'players_out' => $this->getPlayersOut(),
            'players_ico_meals' => $this->getPlayersSubscription(Event::MEALS),
            'players_ico_drinks' => $this->getPlayersSubscription(Event::DRINKS),
            'players_ico_keys' => $this->getPlayersSubscription(Event::KEYS),
        ];
    }
}
