<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PlayerCreateNotification extends Notification
{
    private $user_name;
    private $team_name;
    private $pwd;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user_name, $team_name, $pwd)
    {
        $this->user_name = $user_name;
        $this->team_name = $team_name;
        $this->pwd = $pwd;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $clien_url = env('CLIENT_BASE_URL') . '/signin';
        return (new MailMessage)
            ->subject(__('Bienvenue sur skoreit'))
            ->greeting(__('Hello') . ' ' . ucfirst($this->user_name) . '!')
            ->line( "L'équipe " . ucfirst($this->team_name) . __('app.notification.new_player_added'))
            ->line( !is_null($this->pwd) ? "Voici votre mot de passe provisoire: " : '')
            ->line( !is_null($this->pwd) ? $this->pwd : '')
            ->action('Me connecter', $clien_url)
            ->line(__('app.Regards'))
            ->salutation(config('app.name'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
