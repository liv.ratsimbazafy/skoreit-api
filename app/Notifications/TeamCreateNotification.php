<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TeamCreateNotification extends Notification
{
    use Queueable;

    private $client_url = 'http://127.0.0.1:3000/signin';
    private $user_name;
    private $team_name;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user_name, $team_name)
    {
        $this->user_name = $user_name;
        $this->team_name = $team_name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(__('app.notification.welcome'))
            ->greeting(__('app.hello') . ' ' . ucfirst($this->user_name) . '!')
            ->line(__('app.notification.thanks') . ' "' . $this->team_name . '".')
            ->line(__('app.Regards'))
            ->salutation(config('app.name'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
