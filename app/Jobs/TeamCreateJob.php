<?php

namespace App\Jobs;

use App\Notifications\TeamCreateNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TeamCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $user;
    private $team_name;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $team_name)
    {
        $this->user = $user;
        $this->team_name = $team_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->user->notify(new TeamCreateNotification($this->user->name, $this->team_name));
    }
}
