<?php

namespace App\Jobs;

use App\Notifications\PlayerCreateNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PlayerCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $user;
    private $team_name;
    private $pwd;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $team_name, $pwd)
    {
        $this->user = $user;
        $this->team_name = $team_name;
        $this->pwd = $pwd;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->user->notify(new playerCreateNotification($this->user->name, $this->team_name, $this->pwd));
    }
}
