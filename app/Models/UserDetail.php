<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserDetail extends Model
{
    protected $guarded = [];

    protected $appends = ['role'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function getRoleAttribute()
    {
       return Role::find($this->attributes['role_id'])->name;
    }
}
