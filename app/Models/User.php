<?php

namespace App\Models;

use App\Notifications\EmailVerificationNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'profile_img'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'team_id'
    ];

    public function user_details(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(UserDetail::class);
    }

    public function teams(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Team::class);
    }

    public function coaches(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(TeamDetail::class);
    }

    public function events(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Event::class)->orderByDesc('venue');
    }

    public function event_details(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(EventDetail::class);
    }

    public function getTeamIdAttribute()
    {
        return $this->user_details()->exists() ? $this->user_details()->first()->team_id : '';
    }

    /**
     * override default email verify notification
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new EmailVerificationNotification($this));
    }
}
