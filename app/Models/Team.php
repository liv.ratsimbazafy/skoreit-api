<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Team extends Model
{
    protected $guarded = [];

    protected $appends = ['sport', 'category'];

    protected $hidden = ['types_id', 'type', 'categories_id', 'categories'];


    public function users(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function events(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Event::class)->orderBy('venue', 'DESC');
    }

    public function team_details(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(TeamDetail::class);
    }

    public function categories()
    {
        return $this->belongsTo(Category::class, 'categories_id');
    }

    public function type()
    {
        return $this->belongsTo(Type::class, 'types_id');
    }

    public function getSportAttribute()
    {
        return $this->type->name;
    }

    public function getCategoryAttribute()
    {
        return $this->categories->name;
    }

    public function getUpcomingEvents(): array
    {
        $date_today = Carbon::parse(Carbon::now())->format('Y-m-d');
        $date_next_week = Carbon::parse(Carbon::now()->addDays(7))->format('Y-m-d');

        return $this->events
            ->where('venue', '>=', $date_today)
            ->where('venue', '<=', $date_next_week)
            ->sortByDesc('venue')
            ->values()->all();
    }

    public function getPassedEvents()
    {
        $date_today = Carbon::parse(Carbon::now())->format('Y-m-d');
        return $this->events()
            ->where('venue', '<', $date_today)
            ->paginate(5);
    }

    public function getCurrentSeasonPlayers(bool $page)
    {
        $details = $this->team_details->first();
        $query =  User::whereHas('user_details', function (Builder $query) use($details){
            $query->where('team_id', '=', $this->id)
                ->where('season_start', '=', $details->season_start)
                ->where('season_end', '=', $details->season_end);
        })->orderBy('name', 'ASC');

       if($page) {
           return $query->paginate(5);
       } else {
           return $query->get();
       }
    }
}
