<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamDetail extends Model
{
    protected $guarded = [];

    public function team(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Team::class);
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
