<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    const MEALS = "buy_meals";
    const DRINKS = "buy_drinks";
    const KEYS = "get_keys";

    protected $guarded = [];

    // Relation to eager load
    //public $with = ['event_details'];

    public function users(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function event_details(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(EventDetail::class);
    }

    public function getPlayersSubscription($type): array
    {
        $players = [];
        foreach ($this->event_details->where($type, '=', true) as $event) {
            $players[] =  $event->user;
        }
        return $players;
    }

    public function getPlayersOut()
    {
        $event_id = $this->id;
        $team = Team::find($this->team_id);

        return $team->users()->whereDoesntHave('events', function ($query) use ($event_id) {
            $query->where('events.id', '=', $event_id);
        })->get();
    }
}
