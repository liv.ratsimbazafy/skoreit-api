<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/authenticate', [\App\Http\Controllers\Api\AuthController::class, 'authenticate']);
Route::post('/register', [\App\Http\Controllers\Api\AuthController::class, 'register']);

Route::get('/sports', [\App\Http\Controllers\Api\TeamController::class, 'getTypesAndCategories']);
/**@todo refacto registration* */
Route::post('/team/create', [\App\Http\Controllers\Api\TeamController::class, 'store']);

/**@handle unauthenticated user firing email validation link* */
Route::post('/user/resign', [\App\Http\Controllers\Api\AuthController::class, 'userResignIn']);


//Route::get('/email/verify/{id}/{auth}', [\App\Http\Controllers\Api\EmailVerificationController::class, 'verify']);

Route::group([
    'middleware' => 'auth:sanctum',
    'prefix' => 'team'
], function () {
    Route::get('/{team}', [\App\Http\Controllers\Api\TeamController::class, 'show']);
    Route::patch('/edit/{team}', [\App\Http\Controllers\Api\TeamController::class, 'edit']);
    Route::patch('/season/create/{team}', [\App\Http\Controllers\Api\TeamController::class, 'update']);
    Route::get('/{team}/players', [\App\Http\Controllers\Api\TeamController::class, 'getPlayers']);
    Route::get('/{team_id}/player/{player_id}', [\App\Http\Controllers\Api\TeamController::class, 'getPlayerInfos']);
    Route::post('/players/add', [\App\Http\Controllers\Api\TeamController::class, 'addPlayers']);
    Route::post('/player/remove', [\App\Http\Controllers\Api\TeamController::class, 'removePlayer']);
    Route::post('/player/edit/details', [\App\Http\Controllers\Api\UserController::class, 'updateUserDetails']);
    Route::post('/player/edit/account', [\App\Http\Controllers\Api\UserController::class, 'updateUser']);

    Route::get('/{team_id}/stats', [\App\Http\Controllers\Api\TeamController::class, 'getTeamGlobalStats']);
    Route::post('/contribution/edit', [\App\Http\Controllers\Api\TeamController::class, 'editTeamDetailContribution']);

});

Route::group([
    'middleware' => 'auth:sanctum',
    'prefix' => 'event'
], function () {
    Route::get('/team/{team_id}', [\App\Http\Controllers\Api\EventController::class, 'index']);
    Route::post('/create', [\App\Http\Controllers\Api\EventController::class, 'store']);
    Route::get('/{event}', [\App\Http\Controllers\Api\EventController::class, 'show']);
    Route::post('/edit/{event}', [\App\Http\Controllers\Api\EventController::class, 'update']);
    Route::delete('/team/{team}/delete/{event}', [\App\Http\Controllers\Api\EventController::class, 'destroy']);
    Route::get('/{event}/team/{team}/add-player', [\App\Http\Controllers\Api\EventController::class, 'addPlayerToEvent']);
    Route::get('/{event}/team/{team}/remove-player', [\App\Http\Controllers\Api\EventController::class, 'removePlayerFromEvent']);
    Route::get('/{event}/tasks', [\App\Http\Controllers\Api\EventController::class, 'handleEventTasks']);
    Route::post('/{event}/contribution/add', [\App\Http\Controllers\Api\EventController::class, 'addEventTaskContribution']);
});

Route::group([
    'middleware' => 'auth:sanctum',
    'prefix' => 'user'
], function () {
    Route::post('/password/update', [\App\Http\Controllers\Api\AuthController::class, 'passwordUpdate']);
    Route::post('/avatar/update', [\App\Http\Controllers\Api\AuthController::class, 'profileImageUpdate']);
});
