<?php

use App\Http\Requests\EmailVerificationRequest;
use App\Models\Team;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
   $u = User::find(2);
   return $u->events()->where('events.team_id', 1)->count();
});

/*testing player attendances rate*/
Route::get('/team/{team_id}',  [\App\Http\Controllers\Api\TeamController::class, 'getTeamGlobalStats']);


Route::get('/pwd', function () {
   $p = public_path('users_profile');
   $url = url('/public/users_profile');
   return [$p, $url];
});

Auth::routes(['register' => false, 'verify' => true]);

Route::get('/email/verify/{id}/{hash}', [\App\Http\Controllers\Api\EmailVerificationController::class, 'verify'])->name('verification.verify');
Route::get('/email/resend', [\App\Http\Controllers\Api\EmailVerificationController::class, 'resend'])->name('verification.resend');

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/team/create', [\App\Http\Controllers\Api\TeamController::class, 'store']);
