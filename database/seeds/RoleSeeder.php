<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::insert([
            ['name' => 'entraineur'],
            ['name' => 'entraineur-joueur'],
            ['name' => 'joueur'],
            ['name' => 'intendance'],
        ]);
    }
}
