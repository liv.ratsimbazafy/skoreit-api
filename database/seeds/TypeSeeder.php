<?php

use App\Models\Type;
use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Type::insert([
            ['name' => 'baseball'],
            ['name' => 'basketball'],
            ['name' => 'cycling'],
            ['name' => 'soccer'],
            ['name' => 'handball'],
            ['name' => 'rugby'],
            ['name' => 'volleyball']
        ]);
    }
}
