<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::insert([
            ['name' => 'u 16'],
            ['name' => 'u 18'],
            ['name' => 'u 20'],
            ['name' => 'senior'],
            ['name' => 'loisir'],
        ]);
    }
}
