<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('team_id')->constrained();
            $table->foreignId('role_id')->constrained();
            $table->date('season_start');
            $table->date('season_end');
            $table->integer('position_id')->nullable();
            $table->string('secret')->nullable();
            $table->timestamp('join_on')->nullable();
            $table->integer('current_season')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->integer('height')->nullable();
            $table->integer('weight')->nullable();
            $table->integer('jersey_number')->nullable();
            $table->string('nick_name')->nullable();
            $table->integer('total_attendances')->nullable()->default(0);
            $table->integer('contribution_total')->nullable()->default(0);
            $table->integer('contribution_paid')->nullable()->default(0);
            $table->integer('contribution_unpaid')->nullable()->default(0);
            $table->integer('refund')->nullable()->default(0);
            $table->integer('attendance_rate')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
