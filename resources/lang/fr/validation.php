<?php

return [
    'custom' => [
        'name' => [
            'required' => 'Le champ prénom est requis'
        ],
        'team_name' => [
            'required' => 'Le champ nom de l\'équipe est requis'
        ],
        'sport_type_id' => [
            'required' => 'Le champ sport est requis'
        ],
        'sport_category_id' => [
            'required' => 'Le champ catégorie est requis'
        ],
        'email' => [
            'required' => 'Le champ adresse email est requis',
            'email' => 'Veuillez saisir une adresse email valide !'
        ],
        'password' => [
            'required' => 'Votre mot de passe est requis',
            'min' => 'Mot de passe minimum 5 caractères'
        ],
        'new_password' => [
            'required' => 'Le nouveau mot de passe est requis',
            'min' => 'Nouveau mot de passe minimum 5 caractères'
        ],
        'secret' => [
            'required' => 'Veuillez saisir le code secret',
            'min' => 'Le code secret doit contenir au moins 15 caractères'
        ]
    ],
];
