<?php

return [
    'Regards' => 'Sportivement.',
    'hello' => 'Hello',
    'user_not_found' => 'Le compte avec cette adresse email est introuvable !',
    'wrong_password' => 'Le mot de passe est incorrect !',
    'notification' => [
        'welcome' => 'Bienvenue sur Skore It.',
        'thanks' => 'Merci d\'avoir choisi Skore It pour gérer efficacement votre équipe ',
        'sign_in' => 'Me connecter maintenant',
        'score_it' => 'Score It.',
        'new_player_added' => ' vous a rajouté parmis leurs effectifs!',
        'team_join' => 'Rejoindre l\'équipe'
    ],
    'secret' => [
        'not_valid' => 'Saisir un code secret valide !',
        'account_activation_message' => 'Il semblerai que votre compte n\'a pas encore été activé !',
        'activate_my_account' => 'Activer mon compte'
    ]
];
